# Enroll Instance into AD
## Create DHCP option set
1. In the navigation pane, choose DHCP Options Sets, and then choose Create DHCP options set. 
2. On the Create DHCP options set page, provide the following values for your directory: 
    * For Name, type AWS DS DHCP. 
    * For Domain name, type activedirectory.perengo.com 
    * For Domain name servers, type the IP addresses of your AWS provided directory's DNS servers.(10.102.2.155,10.102.1.244)
    * Leave the settings blank for NTP servers, NetBIOS name servers, and NetBIOS node type. 
3. Choose Create DHCP options set, and then choose Close. The new set of DHCP options appear in your list of DHCP options
4. In the navigation pane, choose Your VPCs. 
5. In the list of VPCs, select AWS DS VPC, choose Actions, and then choose Edit DHCP options set.
6. On the Edit DHCP options set page, select the options set DHCP that you created, and then choose Save.
