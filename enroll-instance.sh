#!/bin/bash

set -x
sudo -i
# Variables
domain_name={{DomainName}} 
user={{User}}
password={{Password}}
filter={{Filter}}

. /etc/os-release
# Install required tools
if [[ "$ID_LIKE" =~ centos|rhel ]]; then
    sudo yum -y install sssd realmd krb5-workstation samba-common-tools
elif [[ "$ID_LIKE" =~ ubuntu|debian ]]; then
    domain_name=$(echo $domain_name | tr '[:lower:]' '[:upper:]')
    DEBIAN_FRONTEND=noninteractive sudo apt-get -y install sssd realmd krb5-user samba-common packagekit
    if ! grep -Fxq '        rdns=false' /etc/krb5.conf; then
        sed -i "/default_realm /a \       \ rdns=false" /etc/krb5.conf;
    fi
    sudo pam-auth-update --enable mkhomedir
fi

# Join instance to realm
realm list | grep 'domain-name: activedirectory.perengo.com' &> /dev/null
if [ $? == 0 ]; then
    echo "Instance already in realm"
else
    yes | sudo realm join -U $user@$domain_name $domain_name --verbose <<< "$password"
fi 

# Allow Password Authentication
sudo sed -i  's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config


cat < /etc/sssd/sssd.conf <<EOM >/etc/sssd/sssd.conf
[sssd]
domains = activedirectory.perengo.com
config_file_version = 2
services = nss, pam

[domain/activedirectory.perengo.com]
ad_domain = activedirectory.perengo.com
krb5_realm = ACTIVEDIRECTORY.PERENGO.COM
realmd_tags = manages-system joined-with-samba 
cache_credentials = True
id_provider = ad
krb5_store_password_if_offline = True
default_shell = /bin/bash   
ldap_id_mapping = True
use_fully_qualified_names = False
fallback_homedir = /home/%u
access_provider = ad
override_homedir=/home/%u
debug_level=9
EOM
# Restart SSH serice
sudo systemctl restart sshd.service
sudo systemctl restart sssd.service

# Add the "AWS Delegated Administrators" group from the domain.
[[ ! -f /etc/sudoers.d/perengo-init-sudo ]] && echo "%AWS\ Delegated\ Administrators@$domain_name ALL=(ALL:ALL) ALL" >> /etc/sudoers.d/perengo-init-sudo
